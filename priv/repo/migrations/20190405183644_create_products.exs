defmodule UncleShop.Repo.Migrations.CreateProducts do
  use Ecto.Migration

  def change do
    create table(:products, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :description, :string
      add :stock, :integer
      add :price, :decimal
      add :attributes, :map
      add :lock_version, :integer

      timestamps()
    end

  end
end
