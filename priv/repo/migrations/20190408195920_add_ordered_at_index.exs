defmodule UncleShop.Repo.Migrations.AddOrderedAtIndex do
  use Ecto.Migration

  def change do
    create index(:orders, [:ordered_at])
  end
end
