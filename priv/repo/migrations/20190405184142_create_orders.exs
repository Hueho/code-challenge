defmodule UncleShop.Repo.Migrations.CreateOrders do
  use Ecto.Migration

  def change do
    create table(:orders, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :buyer_name, :string
      add :ordered_at, :date
      add :state, :string
      add :shipping_cost, :decimal

      timestamps()
    end

  end
end
