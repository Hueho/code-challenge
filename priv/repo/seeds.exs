# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     UncleShop.Repo.insert!(%UncleShop.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias UncleShop.Sales.Product

for _i <- 0..100 do
  UncleShop.Repo.transaction fn ->
    UncleShop.Repo.insert!(%Product{
      name: Faker.Beer.name(),
      description: Faker.Beer.style(),
      price: Faker.Random.Elixir.random_between(1000, 10000) / 100.0,
      stock: Faker.Random.Elixir.random_between(3, 15),
      attributes: %{
        hop: Faker.Beer.hop(),
        alcohol: Faker.Beer.alcohol(),
        brand: Faker.Beer.brand()
      }
    })
  end
end
