# UncleShop

## Pre-requisitos

  * Docker (para inicializar o banco de dados)
  * Elixir 1.8
  * Erlang 21

## Como iniciar o servidor

  * Instalar dependências `mix deps.get`
  * Subir banco com `docker-compose up`
  * Configurar banco com `mix ecto.setup`
    * Serão criados produtos de amostra para testar a aplicação.
  * Subir servidor com `mix phx.server`
  * Documentação da API disponível em http://localhost:4000/swaggerui

## Arquitetura

O código utiliza uma arquitetura simples, em camadas, seguindo boas práticas de Domain-Driven Design.
Utilizando o framework Phoenix, havia a intenção de se separar em múltiplos contextos de negócio, como recomendado
pelos desenvolvedores, entretanto pela simplicidade do projeto acabou-se criando apenas um, o módulo
`UncleShop.Sales`, que centraliza todas as ações de cadastro e recuperação de dados.

A camada de apresentação web é separada em outro módulo dedicado, `UncleShopWeb`, para desacoplar a apresentação
e a lógica.

### Ferramentas utilizadas

A escolha das ferramentas foi feita com motivações técnicas tanto quanto práticas.

- Elixir foi escolhida como a linguagem pelo fato de eu possuir experiência prévia, apreciando muito
sua síntaxe e sua orientação funcional, e porquê é uma das linguagens que fará parte do dia-a-dia na vaga
sendo disputada - faz sentido utilizar ela para se destacar na avaliação, mesmo que eu esteja um pouco enferrujado
e provavelmente os avaliadores vão acabar encontrando erros e código não-idiomático.
- Phoenix foi escolhido como o framework para desenvolvimento também pela experiência prévia, e pelos gerados
de código incluídos que facilitam o processo de _bootstrap_ do processo.
- Para banco de dados foi utlizado o PostgreSQL, novamente por experiência prévia. Um banco de dados relacional se encaixa
bem uma aplicação CRUD com relacionamentos entre entidades muitos-para-muitos (no caso, pedidos com vários produtos e produtos
com vários pedidos).
  - Foi utilizado o Docker Compose para provisionar o banco de dados localmente, pela praticidade.
- Para documentação da API, foi utilizado o formato OpenAPI, já bastante conhecido na indústria e com várias ferramentas
disponíveis de visualização, através de uma biblioteca especializada disponível para Elixir.
  - A geração das rotas da aplicação, porém, foi feita sem o uso da espeficicação: se gerou a especificação após a criação das rotas.

### Comentários adicionais

#### Relacionamento entre pedidos e produtos

Para a camada de persistência, tinham-se duas opções para modelar o relacionamento entre as duas entidades:
- Manter a lista de itens pedidos dentro da entidade de pedidos, como uma entidade embutida (mapeando para
uma coluna do tipo `jsonb` no Postgres)
- Manter uma tabela distinta para items de pedidos, mantendo chaves para o pedido e o produto.

Escolhi a segunda estratégia, para tirar melhor proveito de constraints de chave estrangeira do banco.

#### Performance

Existem algumas oportunidades para melhora de performance que não tive tempo de atacar:
- A atualização do número de produtos após um pedido realizado poderia ser feita em uma query SQL única - atualmente
se fazem múltiplas queries, uma para cada produto afetado.
- A criação de um item de um pedido se faz em duas etapas: uma criando a entidade no banco, e outra atualizando com o preço
atual do produto. Isso não é um problema do ponto de vista da consistência, pelas operações serem feitas na mesma transação,
mas não é um processo ineficiente.

#### Validação dos dados

Todas as validações de dados utilizam a biblioteca Ecto, sugerida para uso pelo Phoenix, em particular
a função de `changesets`, que é provavelmente minha coisa favorita a respeito do Ecto.

#### Chaves primárias

Como é simples de utilizar chaves UUID no Phoenix/Ecto ao invés de chaves inteiras, resolvi fazer para a aplicação ser
menos suscetível a ataques de incrementação de IDs para vazamento de dados.
Mesmo com isso, se não fosse prático utilizar UUID, eu provavelmente usaria algo como [Hashids](https://hashids.org/) para gerar URLs mais agradáveis.