defmodule UncleShopWeb.OrderControllerTest do
  use UncleShopWeb.ConnCase

  alias UncleShop.Sales
  alias UncleShop.Sales.{Order, Product}

  @create_attrs %{
    buyer_name: "some buyer_name",
    shipping_cost: "120.5"
  }
  @update_attrs %{
    buyer_name: "some updated buyer_name",
    shipping_cost: "456.7"
  }
  @invalid_attrs %{buyer_name: nil, ordered_at: nil, shipping_cost: nil}

  def fixture(:order) do
    product = fixture(:product)

    {:ok, order} = Sales.create_order(Map.merge(@create_attrs, %{
      items: [%{product_id: product.id, units: 3}]
    }))

    {order, product}
  end

  def fixture(:product) do
    {:ok, product} = Sales.create_product(%{
      name: Faker.Beer.name(),
      description: Faker.Beer.style(),
      stock: 100,
      price: 10
    })

    product
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all orders", %{conn: conn} do
      conn = get(conn, Routes.order_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create order" do
    setup [:create_product_data]

    test "renders order when data is valid", %{conn: conn, product: %Product{id: id}} do
      conn = post(conn, Routes.order_path(conn, :create), order: Map.merge(@create_attrs, %{
        items: [%{product_id: id, units: 3}]
      }))
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.order_path(conn, :show, id))
      date = current_date_string()

      assert %{
               "id" => id,
               "buyer_name" => "some buyer_name",
               "ordered_at" => ^date,
               "shipping_cost" => "120.5"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.order_path(conn, :create), order: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end

    test "renders errors when item data is invalid", %{conn: conn, product: %Product{id: id}} do
      conn = post(conn, Routes.order_path(conn, :create), order: Map.merge(@create_attrs, %{
        items: [%{product_id: id, units: 30000000}]
      }))
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update order" do
    setup [:create_order_data]

    test "renders order when data is valid", %{conn: conn, order: %Order{id: id} = order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.order_path(conn, :show, id))
      date = current_date_string()

      assert %{
               "id" => id,
               "buyer_name" => "some updated buyer_name",
               "ordered_at" => ^date,
               "shipping_cost" => "456.7"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, order: order} do
      conn = put(conn, Routes.order_path(conn, :update, order), order: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete order" do
    setup [:create_order_data]

    test "deletes chosen order", %{conn: conn, order: order} do
      conn = delete(conn, Routes.order_path(conn, :delete, order))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.order_path(conn, :show, order))
      end
    end
  end

  defp create_order_data(_) do
    {order, product} = fixture(:order)
    {:ok, order: order, product: product}
  end

  defp create_product_data(_) do
    product = fixture(:product)
    {:ok, product: product}
  end

  defp current_date_string() do
    Timex.local
    |> Timex.to_date()
    |> to_string()
  end
end
