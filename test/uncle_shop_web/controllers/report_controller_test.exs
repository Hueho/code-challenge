defmodule UncleShopWeb.ReportControllerTest do
  use UncleShopWeb.ConnCase

  alias UncleShop.Sales

  def order_fixture(attrs \\ %{}) do
    {:ok, order} = Sales.create_order Enum.into(attrs, %{
      buyer_name: Faker.Name.name(),
      shipping_cost: 10.0
    })

    order
  end

  def product_fixture(attrs \\ %{}) do
    {:ok, product} = Sales.create_product Enum.into(attrs, %{
      name: Faker.Beer.name(),
      description: Faker.Beer.style(),
      stock: 100
    })

    product
  end

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "average ticket" do
    setup do
      products = Enum.map(1..4, fn _ -> product_fixture(%{price: 10.0}) end)

      range = Timex.Interval.new(from: ~D[2019-01-01], until: [days: 7])
      orders = for date <- range do
        order_fixture(%{
          ordered_at: date,
          items: Enum.map(products, & %{product_id: &1.id, units: 1})
        })
      end

      {:ok, products: products, orders: orders, from: "2019-01-01", to: "2019-01-07"}
    end

    test "renders report", %{conn: conn, from: from, to: to} do
      conn = get(conn, Routes.report_path(conn, :get_average_ticket), %{from: from, to: to})

      assert %{
               "daily_averages" => [
                  %{"date" => "2019-01-01", "value" => "40.00"},
                  %{"date" => "2019-01-02", "value" => "40.00"},
                  %{"date" => "2019-01-03", "value" => "40.00"},
                  %{"date" => "2019-01-04", "value" => "40.00"},
                  %{"date" => "2019-01-05", "value" => "40.00"},
                  %{"date" => "2019-01-06", "value" => "40.00"},
                  %{"date" => "2019-01-07", "value" => "40.00"},
               ],
               "average" => "40.00"
             } = json_response(conn, 200)
    end

    test "renders empty report", %{conn: conn} do
      conn = get(conn, Routes.report_path(conn, :get_average_ticket), %{from: "2019-01-10", to: "2019-01-14"})
      assert %{
               "daily_averages" => [],
               "average" => nil
             } = json_response(conn, 200)
    end

    test "renders error when dates are invalid", %{conn: conn} do
      conn = get(conn, Routes.report_path(conn, :get_average_ticket), %{from: "20190101", to: "20190107"})
      assert %{} = json_response(conn, 400)
    end
  end
end
