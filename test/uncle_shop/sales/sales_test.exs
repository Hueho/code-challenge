defmodule UncleShop.SalesTest do
  use UncleShop.DataCase

  alias UncleShop.Sales

  describe "products" do
    alias UncleShop.Sales.Product

    @valid_attrs %{attributes: %{}, description: "some description", name: "some name", price: "120.5", stock: 42}
    @update_attrs %{attributes: %{}, description: "some updated description", name: "some updated name", price: "456.7", stock: 43}
    @invalid_attrs %{attributes: nil, description: nil, name: nil, price: nil, stock: nil}

    def product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sales.create_product()

      product
    end

    test "list_products/0 returns all products" do
      product = product_fixture()
      assert Sales.list_products() == [product]
    end

    test "get_product!/1 returns the product with given id" do
      product = product_fixture()
      assert Sales.get_product!(product.id) == product
    end

    test "create_product/1 with valid data creates a product" do
      assert {:ok, %Product{} = product} = Sales.create_product(@valid_attrs)
      assert product.attributes == %{}
      assert product.description == "some description"
      assert product.name == "some name"
      assert product.price == Decimal.new("120.5")
      assert product.stock == 42
    end

    test "create_product/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Sales.create_product(@invalid_attrs)
    end

    test "update_product/2 with valid data updates the product" do
      product = product_fixture()
      assert {:ok, %Product{} = product} = Sales.update_product(product, @update_attrs)
      assert product.attributes == %{}
      assert product.description == "some updated description"
      assert product.name == "some updated name"
      assert product.price == Decimal.new("456.7")
      assert product.stock == 43
    end

    test "update_product/2 with invalid data returns error changeset" do
      product = product_fixture()
      assert {:error, %Ecto.Changeset{}} = Sales.update_product(product, @invalid_attrs)
      assert product == Sales.get_product!(product.id)
    end

    test "delete_product/1 deletes the product" do
      product = product_fixture()
      assert {:ok, %Product{}} = Sales.delete_product(product)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_product!(product.id) end
    end
  end

  describe "orders" do
    alias UncleShop.Sales.Order
    alias UncleShop.Sales.Product

    @valid_attrs %{buyer_name: "some buyer_name", shipping_cost: "120.5"}
    @update_attrs %{buyer_name: "some updated buyer_name", shipping_cost: "456.7"}
    @invalid_attrs %{buyer_name: nil, shipping_cost: nil}

    def order_fixture(attrs \\ %{}) do
      {:ok, order} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Sales.create_order()

      order
    end

    @product_attrs %{
      attributes: %{}, id: Faker.UUID.v4(), description: "some description", name: "some name", price: "120.5", stock: 42}

    def order_product_fixture(attrs \\ %{}) do
      {:ok, product} =
        attrs
        |> Enum.into(@product_attrs)
        |> Sales.create_product()

      product
    end

    test "list_orders/0 returns all orders" do
      product = order_product_fixture()
      order = order_fixture(%{
        items: [%{product_id: product.id, units: 3}]
      })

      assert Sales.list_orders() == [order]
    end

    test "get_order!/1 returns the order with given id" do
      product = order_product_fixture()
      order = order_fixture(%{
        items: [%{product_id: product.id, units: 3}]
      })

      assert Sales.get_order!(order.id) == order
    end

    test "create_order/1 with valid data creates a order" do
      product = order_product_fixture()

      assert {:ok, %Order{} = order} = Sales.create_order(Map.merge(@valid_attrs, %{
        items: [%{product_id: product.id, units: 3}]
      }))
      assert order.buyer_name == "some buyer_name"
      assert order.ordered_at == Timex.to_date(Timex.local())
      assert order.shipping_cost == Decimal.new("120.5")

      [item] = order.items
      assert item.sale_price == product.price
      assert item.units == 3
      assert item.product_id == product.id

      updated_product = Repo.get!(Product, product.id)
      assert updated_product.stock == 39
    end

    test "create_order/1 with invalid data returns error changeset" do
      assert {:error, :order, %Ecto.Changeset{}, %{}} = Sales.create_order(@invalid_attrs)
    end

    test "create_order/1 with invalid item data returns error changeset" do
      product = order_product_fixture()

      assert {:error, {:product, _}, %Ecto.Changeset{}, %{}} = Sales.create_order(Map.merge(@valid_attrs, %{
        items: [%{product_id: product.id, units: product.stock + 1}]
      }))

      assert {:error, :order, %Ecto.Changeset{}, %{}} = Sales.create_order(Map.merge(@valid_attrs, %{
        items: [%{product_id: Faker.UUID.v4(), units: 1}]
      }))
    end

    test "update_order/2 with valid data updates the order" do
      product = order_product_fixture()
      order = order_fixture(%{
        items: [%{product_id: product.id, units: 3}]
      })

      assert {:ok, %Order{} = order} = Sales.update_order(order, @update_attrs)
      assert order.buyer_name == "some updated buyer_name"
      assert order.shipping_cost == Decimal.new("456.7")
    end

    test "update_order/2 with invalid data returns error changeset" do
      product = order_product_fixture()
      order = order_fixture(%{
        items: [%{product_id: product.id, units: 3}]
      })

      assert {:error, :order, %Ecto.Changeset{}, %{}} = Sales.update_order(order, @invalid_attrs)
      assert order == Sales.get_order!(order.id)
    end

    test "delete_order/1 deletes the order" do
      product = order_product_fixture()
      order = order_fixture(%{
        items: [%{product_id: product.id, units: 3}]
      })

      assert {:ok, %Order{}} = Sales.delete_order(order)
      assert_raise Ecto.NoResultsError, fn -> Sales.get_order!(order.id) end
    end
  end

  describe "reports" do
    alias UncleShop.Sales.Order
    alias UncleShop.Sales.Product

    def ready_order_fixture(attrs \\ %{}) do
      {:ok, order} = Sales.create_order Enum.into(attrs, %{
        buyer_name: Faker.Name.name(),
        shipping_cost: 10.0
      })

      order
    end

    def ready_product_fixture(attrs \\ %{}) do
      {:ok, product} = Sales.create_product Enum.into(attrs, %{
        name: Faker.Beer.name(),
        description: Faker.Beer.style(),
        stock: 100
      })

      product
    end

    test "create average ticket report" do
      products = for _i <- 0..3 do
        ready_product_fixture(%{price: 10.0});
      end

      range = Timex.Interval.new(from: ~D[2019-01-01], until: [days: 7])

      for date <- range do
        for _i <- 0..1 do
          ready_order_fixture(%{
            ordered_at: date,
            items: Enum.map(products, fn product ->
              %{product_id: product.id, units: 2}
            end)
          })
        end
      end

      expected = Decimal.new("80.00")

      assert Sales.get_average_ticket(~D[2019-01-01], ~D[2019-01-07]) == %{
        daily_averages: [
          %{date: ~D[2019-01-01], value: expected},
          %{date: ~D[2019-01-02], value: expected},
          %{date: ~D[2019-01-03], value: expected},
          %{date: ~D[2019-01-04], value: expected},
          %{date: ~D[2019-01-05], value: expected},
          %{date: ~D[2019-01-06], value: expected},
          %{date: ~D[2019-01-07], value: expected},
        ],
        average: expected
      }
    end
  end
end
