defmodule Mix.Tasks.UncleShop.OpenApi do
  def run([output_file]) do
    json = Jason.encode!(UncleShopWeb.ApiSpec.spec(), pretty: true)
    :ok = File.write!(output_file, json)
  end
end
