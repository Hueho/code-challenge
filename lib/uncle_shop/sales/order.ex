defmodule UncleShop.Sales.Order do
  use Ecto.Schema
  use Timex
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "orders" do
    field :buyer_name, :string
    field :state, :string, default: "pending"
    field :ordered_at, :date
    field :shipping_cost, :decimal

    has_many :items, UncleShop.Sales.OrderItem, on_delete: :delete_all
    has_many :products, through: [:items, :product]

    timestamps()
  end

  @valid_states ["pending", "approved", "delivered", "canceled"]
  @valid_state_transitions %{
    "pending" => ["approved", "canceled"],
    "approved" => ["delivered", "canceled"],
    "canceled" => []
  }

  @doc "Changeset for creating new orders"
  def create_changeset(order, attrs) do
    # TODO: should handle timezones better
    current_date = Timex.to_date Timex.local

    order
    |> cast(attrs, [:buyer_name, :ordered_at, :shipping_cost])
    |> validate_required([:buyer_name, :shipping_cost])
    |> set_ordered_at(current_date)
    |> cast_assoc(:items, required: true)
  end

  @doc "Changeset for updating orders"
  def update_changeset(order, attrs) do
    order
    |> cast(attrs, [:buyer_name, :state, :shipping_cost])
    |> validate_required([:buyer_name, :shipping_cost])
    |> validate_inclusion(:state, @valid_states)
    |> validate_state_change()
  end

  defp set_ordered_at(changeset, date) do
    # Respect if ordered_at was set already
    put_change(changeset, :ordered_at, get_change(changeset, :ordered_at, date))
  end

  defp validate_state_change(changeset) do
    case {get_change(changeset, :state), changeset.data.state} do
      {nil, _} ->
        changeset # no change
      {_, nil} ->
        changeset # invalid record, should not happen
      {same, same} ->
        changeset # no actual change
      {to, from} ->
        # check for valid transition
        if to not in @valid_state_transitions[from] do
          [title: "invalid transtion from #{from} to #{to}"]
        else
          []
        end
    end
  end
end
