defmodule UncleShop.Sales.Product do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "products" do
    field :attributes, :map, default: %{}
    field :description, :string
    field :name, :string
    field :price, :decimal
    field :stock, :integer
    field :lock_version, :integer, default: 1

    has_many :order_items, UncleShop.Sales.OrderItem, on_delete: :delete_all
    has_many :orders, through: [:order_items, :order]

    timestamps()
  end

  @doc false
  def changeset(product, attrs) do
    product
    |> cast(attrs, [:name, :description, :stock, :price, :attributes])
    |> validate_required([:name, :description, :stock, :price])
    |> validate_number(:stock, greater_than_or_equal_to: 0)
    |> validate_number(:price, greater_than: 0)
    |> optimistic_lock(:lock_version)
  end

  @doc "Creates a changeset for use when creating or updating orders"
  def order_changeset(product, units_sold) do
    product
    |> change(stock: product.stock - units_sold)
    |> validate_number(:stock, greater_than_or_equal_to: 0)
    |> optimistic_lock(:lock_version)
  end
end
