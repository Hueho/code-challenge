defmodule UncleShop.Sales.OrderItem do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "order_items" do
    field :sale_price, :decimal
    field :units, :integer

    belongs_to :order, UncleShop.Sales.Order
    belongs_to :product, UncleShop.Sales.Product

    timestamps()
  end

  @doc false
  def changeset(order_item, attrs) do
    order_item
    |> cast(attrs, [:units, :product_id])
    |> validate_required([:units, :product_id])
    |> assoc_constraint(:order)
    |> assoc_constraint(:product)
  end

  @doc "Changeset for setting the sale price for a item"
  def price_changeset(order_item, sale_price) do
    order_item
    |> change()
    |> put_change(:sale_price, sale_price)
    |> validate_number(:sale_price, greater_than: 0)
  end
end
