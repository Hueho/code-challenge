defmodule UncleShop.Repo do
  use Ecto.Repo,
    otp_app: :uncle_shop,
    adapter: Ecto.Adapters.Postgres
end
