defmodule UncleShop.Sales do
  @moduledoc """
  The Sales context.
  """

  import Ecto.Query, warn: false
  alias Ecto.Multi

  alias UncleShop.Repo

  alias UncleShop.Sales.Product

  @doc """
  Returns the list of products.
  """
  def list_products do
    Repo.all(Product)
  end

  @doc """
  Gets a single product.

  Raises `Ecto.NoResultsError` if the Product does not exist.
  """
  def get_product!(id), do: Repo.get!(Product, id)

  @doc """
  Creates a product.
  """
  def create_product(attrs \\ %{}) do
    %Product{}
    |> Product.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a product.
  """
  def update_product(%Product{} = product, attrs) do
    product
    |> Product.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Product.
  """
  def delete_product(%Product{} = product) do
    Repo.delete(product)
  end

  alias UncleShop.Sales.Order
  alias UncleShop.Sales.OrderItem

  @doc """
  Returns the list of orders.
  """
  def list_orders do
    Repo.all(from Order, preload: [items: :product])
  end

  @doc """
  Gets a single order.

  Raises `Ecto.NoResultsError` if the Order does not exist.
  """
  def get_order!(id), do: Repo.get!(Order, id) |> Repo.preload([items: :product])

  @doc """
  Creates a order.

  Requires all ordered itens to be included.
  """
  def create_order(attrs \\ %{}) do
    result = Multi.new()
    |> Multi.insert(:order, Order.create_changeset(%Order{}, attrs))
    |> Multi.merge(fn %{order: order} ->
      # Get items and products being changed
      items = Repo.all from(Ecto.assoc(order, :items), preload: :product)

      # Update items with original product price
      items_multi = Enum.reduce items, Multi.new, fn item, last_multi ->
        Multi.update(last_multi, {:item, item.id}, OrderItem.price_changeset(item, item.product.price))
      end

      # Update each product stock from order
      Enum.reduce items, items_multi, fn item, last_multi ->
        Multi.update(last_multi, {:product, item.product.id}, Product.order_changeset(item.product, item.units))
      end
    end)
    |> Repo.transaction()

    case result do
      {:ok, %{order: order}} -> {:ok, Repo.preload(order, [items: :product], force: true)}
      err -> err
    end
  end

  @doc """
  Updates a order.

  Cannot change ordered itens.
  """
  def update_order(%Order{} = order, attrs) do
    result = Multi.new()
    |> Multi.update(:order, Order.update_changeset(order, attrs))
    |> Multi.merge(fn %{order: order} ->
      next = Multi.new()

      if order.state == "cancelled" do
        # Give stock back to products
        items = Repo.all from(Ecto.assoc(order, :items), preload: :product)

        Enum.reduce items, next, fn item, last_multi ->
          Multi.update(last_multi, {:product, item.product.id}, Product.order_changeset(item.product, -item.units))
        end
      else
        next
      end
    end)
    |> Repo.transaction()

    case result do
      {:ok, %{order: order}} -> {:ok, Repo.preload(order, [items: :product], force: true)}
      err -> err
    end
  end

  @doc """
  Deletes a Order.

  Reverts all stock alterations.
  """
  def delete_order(%Order{} = order) do
    # Give stock back to products
    items = Repo.all Ecto.Query.from(Ecto.assoc(order, :items), preload: :product)

    multi = Enum.reduce items, Multi.new(), fn item, last_multi ->
      Multi.update(last_multi, {:product, item.product.id}, Product.order_changeset(item.product, -item.units))
    end

    result = multi
    |> Multi.delete(:order, order)
    |> Repo.transaction()

    case result do
      {:ok, %{order: order}} -> {:ok, order}
      err -> err
    end
  end

  @doc """
  Retrieve the average ticket for a given date range
  """
  def get_average_ticket(from, to) do
    # First get full value for each order
    order_values = from o in Order,
      join: i in assoc(o, :items),
      where: o.ordered_at >= ^from and o.ordered_at <= ^to,
      group_by: o.id,
      select: %{id: o.id, date: o.ordered_at, value: fragment("round(?, 2)", sum(i.sale_price * i.units))}

    # Get average ticket by day
    aggregated_by_day = from v in subquery(order_values),
      group_by: v.date, order_by: [asc: v.date],
      select: %{date: v.date, value: fragment("round(?, 2)", sum(v.value) / count())}
    by_day = Repo.all(aggregated_by_day)

    # Get average for whole range
    aggregated_by_range = from v in subquery(aggregated_by_day), select: fragment("round(?, 2)", avg(v.value))
    by_range = Repo.all(aggregated_by_range)

    %{
      daily_averages: by_day,
      average: case by_range do
        [value] -> value
        [] -> nil
      end
    }
  end
end
