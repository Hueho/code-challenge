defmodule UncleShopWeb.ProductController do
  use UncleShopWeb, :controller

  alias UncleShop.Sales
  alias UncleShop.Sales.Product

  action_fallback UncleShopWeb.FallbackController

  def index(conn, _params) do
    products = Sales.list_products()
    render(conn, "index.json", products: products)
  end

  def create(conn, %{"product" => product_params}) do
    with {:ok, %Product{} = product} <- Sales.create_product(product_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.product_path(conn, :show, product))
      |> render("show.json", product: product)
    end
  end

  def show(conn, %{"id" => id}) do
    product = Sales.get_product!(id)
    render(conn, "show.json", product: product)
  end

  def update(conn, %{"id" => id, "product" => product_params}) do
    product = Sales.get_product!(id)

    with {:ok, %Product{} = product} <- Sales.update_product(product, product_params) do
      render(conn, "show.json", product: product)
    end
  end

  def delete(conn, %{"id" => id}) do
    product = Sales.get_product!(id)

    with {:ok, %Product{}} <- Sales.delete_product(product) do
      send_resp(conn, :no_content, "")
    end
  end

  defmodule Operations do
    alias UncleShopWeb.Schemas
    alias OpenApiSpex.{Operation, RequestBody, MediaType, Response}

    def index() do
      %Operation{
        tags: ["products"],
        summary: "List products",
        description: "List all registered products",
        operationId: "ProductController.index",
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.ProductsResponse)
        }
      }
    end

    def create() do
      %Operation{
        tags: ["products"],
        summary: "Create product",
        description: "Create a new product",
        operationId: "ProductController.create",
        requestBody: %RequestBody{
          required: true,
          content: %{
            "application/json" => %MediaType{
              schema: Schemas.Product
            }
          }
        },
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.ProductResponse),
          400 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def show() do
      %Operation{
        tags: ["products"],
        summary: "Show product",
        description: "Show a product by ID",
        operationId: "ProductController.show",
        parameters: [
          Operation.parameter(:id, :path, :string, "Product ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.ProductResponse),
          404 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def update() do
      %Operation{
        tags: ["products"],
        summary: "Update product",
        description: "Update an new product",
        operationId: "ProductController.update",
        parameters: [
          Operation.parameter(:id, :path, :string, "Product ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        requestBody: %RequestBody{
          required: true,
          content: %{
            "application/json" => %MediaType{
              schema: Schemas.Product
            }
          }
        },
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.ProductResponse),
          400 => Operation.response("Error", "application/json", Schemas.ErrorResponse),
          404 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def delete() do
      %Operation{
        tags: ["products"],
        summary: "Delete product",
        description: "Delete a product by ID",
        operationId: "ProductController.delete",
        parameters: [
          Operation.parameter(:id, :path, :string, "Product ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        responses: %{
          200 => %Response{description: "Product deleted with success"},
        }
      }
    end
  end

  def open_api_operation(action) do
    apply(Operations, action, [])
  end
end
