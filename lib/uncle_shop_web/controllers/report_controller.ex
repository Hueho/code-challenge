defmodule UncleShopWeb.ReportController do
  use UncleShopWeb, :controller
  alias UncleShop.Sales

  action_fallback UncleShopWeb.FallbackController

  def get_average_ticket(conn, %{"from" => from, "to" => to}) do
    case {parse_date(from), parse_date(to)} do
      {:error, _} -> {:error, :bad_request}
      {_, :error} -> {:error, :bad_request}
      {parsed_from, parsed_to} ->
        json(conn, Sales.get_average_ticket(parsed_from, parsed_to))
    end
  end

  defmodule Operations do
    alias UncleShopWeb.Schemas
    alias OpenApiSpex.Operation

    def get_average_ticket() do
      %Operation{
        tags: ["products"],
        summary: "Get average ticket",
        description: "Get the average ticket values for the given date range",
        operationId: "ReportController.get_average_ticket",
        parameters: [
          Operation.parameter(:from, :query, :string, "From date (inclusive)", format: :date, example: "2019-01-01"),
          Operation.parameter(:to, :query, :string, "To date (inclusive)", format: :date, example: "2019-01-07"),
        ],
        responses: %{
          200 => Operation.response("Report", "application/json", Schemas.ReportResponse),
          404 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end
  end

  def open_api_operation(action) do
    apply(Operations, action, [])
  end


  defp parse_date(str) do
    case Timex.parse(str, "{YYYY}-{0M}-{0D}") do
      {:ok, datetime} -> Timex.to_date(datetime)
      {:error, _} -> :error
    end
  end
end
