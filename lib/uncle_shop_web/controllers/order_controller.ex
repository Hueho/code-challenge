defmodule UncleShopWeb.OrderController do
  use UncleShopWeb, :controller

  alias UncleShop.Sales
  alias UncleShop.Sales.Order

  action_fallback UncleShopWeb.FallbackController

  def index(conn, _params) do
    orders = Sales.list_orders()
    render(conn, "index.json", orders: orders)
  end

  def create(conn, %{"order" => order_params}) do
    with {:ok, %Order{} = order} <- Sales.create_order(order_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.order_path(conn, :show, order))
      |> render("show.json", order: order)
    end
  end

  def show(conn, %{"id" => id}) do
    order = Sales.get_order!(id)
    render(conn, "show.json", order: order)
  end

  def update(conn, %{"id" => id, "order" => order_params}) do
    order = Sales.get_order!(id)

    with {:ok, %Order{} = order} <- Sales.update_order(order, order_params) do
      render(conn, "show.json", order: order)
    end
  end

  def delete(conn, %{"id" => id}) do
    order = Sales.get_order!(id)

    with {:ok, %Order{}} <- Sales.delete_order(order) do
      send_resp(conn, :no_content, "")
    end
  end

  defmodule Operations do
    alias UncleShopWeb.Schemas
    alias OpenApiSpex.{Operation, RequestBody, MediaType, Response}

    def index() do
      %Operation{
        tags: ["orders"],
        summary: "List orders",
        description: "List all registered orders",
        operationId: "OrderController.index",
        responses: %{
          200 => Operation.response("Order", "application/json", Schemas.OrdersResponse)
        }
      }
    end

    def create() do
      %Operation{
        tags: ["orders"],
        summary: "Create order",
        description: "Create an new order",
        operationId: "OrderController.create",
        requestBody: %RequestBody{
          required: true,
          content: %{
            "application/json" => %MediaType{
              schema: Schemas.Order
            }
          }
        },
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.ProductResponse),
          400 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def show() do
      %Operation{
        tags: ["orders"],
        summary: "Show order",
        description: "Show an order by ID",
        operationId: "OrderController.show",
        parameters: [
          Operation.parameter(:id, :path, :string, "Order ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.OrderResponse),
          404 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def update() do
      %Operation{
        tags: ["orders"],
        summary: "Update product",
        description: "Update an order",
        operationId: "OrderController.update",
        parameters: [
          Operation.parameter(:id, :path, :string, "Order ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        requestBody: %RequestBody{
          required: true,
          content: %{
            "application/json" => %MediaType{
              schema: Schemas.Order
            }
          }
        },
        responses: %{
          200 => Operation.response("Product", "application/json", Schemas.OrderResponse),
          400 => Operation.response("Error", "application/json", Schemas.ErrorResponse),
          404 => Operation.response("Error", "application/json", Schemas.ErrorResponse)
        }
      }
    end

    def delete() do
      %Operation{
        tags: ["orders"],
        summary: "Delete order",
        description: "Delete an order by ID",
        operationId: "OrderController.delete",
        parameters: [
          Operation.parameter(:id, :path, :string, "Order ID", format: :uuid, example: "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5")
        ],
        responses: %{
          200 => %Response{description: "Product deleted with success"},
        }
      }
    end
  end

  def open_api_operation(action) do
    apply(Operations, action, [])
  end

end
