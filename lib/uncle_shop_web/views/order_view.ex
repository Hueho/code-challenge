defmodule UncleShopWeb.OrderView do
  use UncleShopWeb, :view
  alias UncleShopWeb.OrderView
  alias UncleShopWeb.OrderItemView

  def render("index.json", %{orders: orders}) do
    %{data: render_many(orders, OrderView, "order.json")}
  end

  def render("show.json", %{order: order}) do
    %{data: render_one(order, OrderView, "order.json")}
  end

  def render("order.json", %{order: order}) do
    %{id: order.id,
      buyer_name: order.buyer_name,
      ordered_at: order.ordered_at,
      state: order.state,
      shipping_cost: order.shipping_cost,
      items: render_many(order.items, OrderItemView, "order_item.json")}
  end
end
