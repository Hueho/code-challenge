defmodule UncleShopWeb.Router do
  use UncleShopWeb, :router

  pipeline :api do
    plug :accepts, ["json"]

    plug OpenApiSpex.Plug.PutApiSpec, module: UncleShopWeb.ApiSpec
  end

  pipeline :browser do
    plug :accepts, ["html"]
  end

  scope "/api" do
    pipe_through :api

    resources "/products", UncleShopWeb.ProductController, except: [:new, :edit]
    resources "/orders", UncleShopWeb.OrderController, except: [:new, :edit]

    scope "/reports" do
      get "/average_ticket", UncleShopWeb.ReportController, :get_average_ticket
    end

    get "/openapi", OpenApiSpex.Plug.RenderSpec, []
  end

  scope "/" do
    pipe_through :browser

    get "/swaggerui", OpenApiSpex.Plug.SwaggerUI, path: "/api/openapi"
  end
end
