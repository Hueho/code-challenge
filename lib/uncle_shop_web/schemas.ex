defmodule UncleShopWeb.Schemas do
  alias OpenApiSpex.Schema

  defmodule Product do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "Product",
      description: "A registered product in the shop",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Product ID", format: :uuid},
        name: %Schema{type: :string, description: "Product name"},
        description: %Schema{type: :string, description: "Product description"},
        stock: %Schema{type: :integer, description: "Units available"},
        price: %Schema{type: :string, description: "Current price", format: :currency},
        attributes: %Schema{type: :object, description: "Custom attributes for product"},
        inserted_at: %Schema{type: :string, description: "Creation timestamp", format: :datetime},
        updated_at: %Schema{type: :string, description: "Update timestamp", format: :datetime}
      },
      required: [:name, :description, :stock, :price],
      example: %{
        "id" => "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5",
        "name" => "Staipava Stout",
        "description" => "Exclusive stout beer from Staipava",
        "stock" => 5,
        "price" => "25.99",
        "attributes" => %{
          "abl" => "9%"
        }
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule ProductResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "ProductResponse",
      description: "Response schema for a single product",
      type: :object,
      properties: %{
        data: Product
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule ProductsResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "ProductResponse",
      description: "Response schema for multiple products",
      type: :object,
      properties: %{
        data: %Schema{type: :array, description: "Product data", items: Product}
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule OrderItem do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "Product",
      description: "One item from a specific order",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Item ID", format: :uuid},
        order_id: %Schema{type: :string, description: "Order ID", format: :uuid},
        product_id: %Schema{type: :string, description: "Product ID", format: :uuid},
        units: %Schema{type: :integer, description: "Amount ordered"},
        sale_price: %Schema{type: :string, description: "Price at time of order", format: :currency},
        inserted_at: %Schema{type: :string, description: "Creation timestamp", format: :datetime},
        updated_at: %Schema{type: :string, description: "Update timestamp", format: :datetime}
      },
      required: [:order_id, :product_id, :units],
      example: %{
        "id" => "5693b189-eea9-4029-980c-3e022e1803c0",
        "order_id" => "124086aa-9597-4435-b8cd-1d7d454573c5",
        "product_id" => "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5",
        "units" => 2,
        "sale_price" => "25.99"
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule Order do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "Order",
      description: "A registered order in the shop",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Item ID", format: :uuid},
        buyer_name: %Schema{type: :string, description: "Buyer's name"},
        state: %Schema{type: :string, description: "Order status", enum: ["pending", "approved", "delivered", "cancelled"]},
        shipping_cost: %Schema{type: :string, description: "Shipping costs for order", format: :currency},
        ordered_at: %Schema{type: :string, description: "Date where order was made", format: :date},
        inserted_at: %Schema{type: :string, description: "Creation timestamp", format: :datetime},
        updated_at: %Schema{type: :string, description: "Update timestamp", format: :datetime},
        items: %Schema{type: :array, items: %Schema{type: OrderItem}, description: "Items composing order"}
      },
      required: [:buyer_name, :shipping_cost],
      example: %{
        "id" => "124086aa-9597-4435-b8cd-1d7d454573c5",
        "buyer_name" => "Buyer McBuy",
        "shipping_cost" => "1.99",
        "state" => "pending",
        "items" => [
          %{
            "id" => "5693b189-eea9-4029-980c-3e022e1803c0",
            "order_id" => "124086aa-9597-4435-b8cd-1d7d454573c5",
            "product_id" => "ad8a9ac7-1d9d-44f4-9023-af6df7473cd5",
            "units" => 2,
            "sale_price" => "25.99"
          }
        ]
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule OrderResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "OrderResponse",
      description: "Response schema for a single order",
      type: :object,
      properties: %{
        data: Order
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule OrdersResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "OrdersResponse",
      description: "Response schema for multiple order",
      type: :object,
      properties: %{
        data: %Schema{type: :array, items: Order, description: "Order data"}
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule ErrorResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "ErrorResponse",
      description: "Response schema for errors",
      type: :object,
      properties: %{
        errors: %Schema{type: :array, items: %Schema{type: :object}, description: "List of errors"}
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end

  defmodule ReportResponse do
    @behaviour OpenApiSpex.Schema
    @derive [Jason.Encoder]
    @schema %Schema{
      title: "ReportResponse",
      description: "Response schema for average ticket reports",
      type: :object,
      properties: %{
        average: %Schema{type: :string, description: "Average over whole period", format: :currency},
        daily_averages: %Schema{type: :array, description: "List of errors", items: %Schema{
          type: :object,
          properties: %{
            date: %Schema{type: :string, description: "Analyzed date", format: :date},
            value: %Schema{type: :string, description: "Average over given date", format: :currency}
          }
        }}
      },
      example: %{
        "average" => "20.00",
        "daily_averages" => [
          %{"date" => "2019-01-01", "value" => "10.00"},
          %{"date" => "2019-01-02", "value" => "20.00"},
          %{"date" => "2019-01-03", "value" => "30.00"},
        ]
      },
      "x-struct": __MODULE__
    }

    def schema, do: @schema
    defstruct Map.keys(@schema.properties)
  end
end
