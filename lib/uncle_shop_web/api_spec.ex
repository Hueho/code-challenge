defmodule UncleShopWeb.ApiSpec do
  alias OpenApiSpex.{OpenApi, Server, Info, Paths}

  def spec do
    OpenApiSpex.resolve_schema_modules %OpenApi{
      servers: [
        # Populate the Server info from a phoenix endpoint
        Server.from_endpoint(UncleShopWeb.Endpoint, otp_app: :uncle_shop)
      ],
      info: %Info{
        title: "UncleShop",
        version: "1.0"
      },
      # populate the paths from a phoenix router
      paths: Paths.from_router(UncleShopWeb.Router)
    }
  end
end
